import{Routes,RouterModule}from '@angular/router';
import { BooksModule } from './books/books.module';
import {UsersModule} from './users/users.module';
import {LibraryModule} from './library/library.module';
const moduleRoutes:Routes=[
    {
        path : 'books',
        loadChildren :'./books/books.module#BooksModule'
    },
    {
        path :'users',
        loadChildren : './users/users.module#UsersModule'
    },
    {
        path : 'library',
        loadChildren : './library/library.module#LibraryModule'
    }
];
export const AppModuleRoutes=RouterModule.forRoot(moduleRoutes);