import {RouterModule, Routes} from'@angular/router';
import { UserlistComponent } from'./userlist/userlist.component';
 
const userRoutes : Routes = [
    {
path :'',
component :UserlistComponent
    }
]
 
export const UserRoutesModule = RouterModule.forChild(userRoutes);

