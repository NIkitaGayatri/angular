import { Component, OnInit } from '@angular/core';
import {UserserviceService} from '../userservice.service';
import {User} from '../user';
import { ApphttpclientService } from 'src/app/apphttpclient.service';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {

  constructor(private userService : UserserviceService,private httpClientService :ApphttpclientService) { }
  users : User[];
  selectedUser :User=new User();
  isEdit :boolean =false;
  ngOnInit(): void {
    this.getUserList();
  }
  getUserList(){
    this.userService.getAllUsers().subscribe((res : User[])=>{
      this.users = res;
    });
  }
  parentMethodAck(val){
    if(val=="success"){
      this.getUserList();
    }
    document.getElementById("closePopUp").click();
    console.log("acknowleded by child");
  }
  createUser(){
    this.isEdit=false;
    this.selectedUser=new User();
  }
  updateUser(user){
    this.selectedUser=user;
    this.isEdit=true;
  }
  deleteUser(user){
    this.httpClientService.delete("users/"+user.userId).subscribe(res=>{
      console.log("deleted User");
      this.getUserList();
    });
  }
}
