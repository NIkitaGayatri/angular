import { Injectable } from '@angular/core';
import { ApphttpclientService } from '../apphttpclient.service';

@Injectable({
  providedIn: 'root'
})
export class UserserviceService {

  constructor(private httpClientService:ApphttpclientService) { }
 
 
  getAllUsers(){
    return this.httpClientService.get('users');
  }
}
