import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserlistComponent } from './userlist/userlist.component';
import {UserRoutesModule} from './users.routes';
import {RouterModule}from '@angular/router';
import { UserComponent } from './user/user.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [UserlistComponent, UserComponent],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    UserRoutesModule
  ]
})
export class UsersModule { }
