import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { ApphttpclientService } from 'src/app/apphttpclient.service';
import { User } from '../user';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  @Input()
  user : User;
  @Input()
  editable: boolean;
  @Output()
  successEvent = new EventEmitter<any>();

  constructor(private httpClientService : ApphttpclientService) { }

  ngOnInit(): void {
  }
  
  saveUser(){
    if(this.editable){
    this.httpClientService.put("users/"+this.user.userId,this.user).subscribe(res=>{
      console.log("edit success");
      this.successEvent.emit("success");
    });
  }else{
  this.httpClientService.post("users",this.user).subscribe(res=>{
    console.log("user created successfully");
    this.successEvent.emit("success");
  });

}
  }
}

