import { Component, OnInit, Input } from '@angular/core';
import { LibraryPojo } from '../librarypojo';
import { ApphttpclientService } from 'src/app/apphttpclient.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-librarybook',
  templateUrl: './librarybook.component.html',
  styleUrls: ['./librarybook.component.css']
})
export class LibrarybookComponent implements OnInit {

  constructor(private httpClientService :ApphttpclientService) { }
  @Input()
  id:number;
  @Input()
  userId:number;
  error : string;
libraryEntry:LibraryPojo;
  ngOnInit(): void {
  }
  issueBookToUser(libraryform : NgForm){
    document.getElementById("closePopUP").click();
    console.log("user Id"+this.userId);
    console.log("book Id"+this.id);
    this.httpClientService.post("users/"+this.userId+"/books/"+this.id,this.libraryEntry).subscribe(
      res=>{
        console.log("new entry in libarary created");
        alert("Book Issued to user succefully");
      },err=>{
        this.error=err;
        console.log("error in library"+this.error);
      }
      
    );
  }

}
