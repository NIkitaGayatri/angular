import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LibrarylistComponent } from './librarylist/librarylist.component';
import{LibraryRoutes}from './library.routes';
import { LibraryComponent } from './library/library.component';
import { LibrarybookComponent } from './librarybook/librarybook.component';



@NgModule({
  declarations: [LibrarylistComponent, LibraryComponent, LibrarybookComponent],
  imports: [
    CommonModule,
    FormsModule,
    LibraryRoutes
  ]
})
export class LibraryModule { }
