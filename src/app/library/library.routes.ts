import {Routes,RouterModule}from '@angular/router';
import{LibrarylistComponent}from './librarylist/librarylist.component';

const libraryRoutes : Routes =[
    {
    path : '',
    component : LibrarylistComponent
    }
];
export const LibraryRoutes=RouterModule.forChild(libraryRoutes);