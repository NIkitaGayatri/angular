import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/users/user';
import { ApphttpclientService } from 'src/app/apphttpclient.service';
import { LibraryPojo } from '../librarypojo';

@Component({
  selector: 'app-librarylist',
  templateUrl: './librarylist.component.html',
  styleUrls: ['./librarylist.component.css']
})
export class LibrarylistComponent implements OnInit {
  selectedUser : number=0;
  libraryEntries : LibraryPojo[];
  error:string;
  columns :["NAME","AUTHOR","DESCRIPTION","#DELETE"];
  constructor(private httpClientService : ApphttpclientService) { }

  ngOnInit(): void {
  }
  getAllBooksCorrespondingToUser(userId){
    this.selectedUser=userId;
    console.log("In librarylist"+this.selectedUser);
    document.getElementById("closePopUp").click();
    this.httpClientService.get("users/"+this.selectedUser).subscribe((res:LibraryPojo[])=>{
        this.libraryEntries=res;
        console.log("In libarary"+this.libraryEntries[0].userId+this.libraryEntries[0].id);
    },
    err=>{
      this.error=err;
      console.log("error in library"+this.error);
    }
    );
  }

  deleteBookCorrespondingToUser(library){
    console.log(library.userId);
    console.log("In delete"+this.selectedUser);
    this.httpClientService.delete("users/"+this.selectedUser+"/books/"+library.id).subscribe(res=>{
      console.log("book corresponding to user deleted");
      this.getAllBooksCorrespondingToUser(this.selectedUser);
    });
  }

}
