import { Component, OnInit, Output, Input,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-library',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.css']
})
export class LibraryComponent implements OnInit {

  constructor() { }
  @Input()
  userId : number;
  @Output()
  validUserId =new EventEmitter<any>();
  ngOnInit(): void {
  }
  saveUserId(){
    this.validUserId.emit(this.userId);
  }
  
}
