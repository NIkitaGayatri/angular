import { Injectable } from '@angular/core';
import {environment} from '../environments/environment';
import { HttpClient, HttpClientModule } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ApphttpclientService {

  constructor(private http : HttpClient) { }
  domain = environment.baseURI;


  public get(endpoint){
    return this.http.get(this.domain + endpoint);
  }
  public post(endpoint,bodyparams){
    return this.http.post(this.domain+endpoint,bodyparams);
  }
  public put(endpoint,bodyparams){
    return this.http.put(this.domain+endpoint,bodyparams);
  }
  public delete(endpoint){
    return this.http.delete(this.domain+endpoint);
  }
}
