import { Routes, RouterModule } from "@angular/router";
import { BooklistComponent } from './booklist/booklist.component';

const booksRoutes : Routes = [
    {
        path:'',
        component : BooklistComponent
    }
];

export const BookComponentRoutes = RouterModule.forChild(booksRoutes);
