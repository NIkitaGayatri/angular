import { Component, OnInit } from '@angular/core';
import { ApphttpclientService } from 'src/app/apphttpclient.service';
import {Book} from '../book';
import { BooksService } from '../books.service';

@Component({
  selector: 'app-booklist',
  templateUrl: './booklist.component.html',
  styleUrls: ['./booklist.component.css']
})
export class BooklistComponent implements OnInit {

  constructor(private bookService : BooksService, private httpClientService : ApphttpclientService) { }
books:Book[];
selectedBook :Book=new Book();
isEdit : boolean = false;
error :string;
columns :["ID","NAME","AUTHOR","DESCRIPTION","#EDIT","#DELETE"];
  ngOnInit() {
    this.getAllBooks();
  }
getAllBooks(){
   this.bookService.getBooksList().subscribe((res:Book[])=>{
    this.books=res;
},
err=>{
  this.error=err;
});
}
parentMethodAck(val){
  if(val=="success"){
    this.getAllBooks();
  }
  document.getElementById("closePopUp").click();
  console.log("acknowleded by child");
}
updateBook(book){
  this.selectedBook=book;
  this.isEdit=true;
}
createBook(){
  this.isEdit=false;
}
deleteBook(book){
  this.httpClientService.delete("book/"+book.id).subscribe(res=>{
    console.log("book deleted");
    this.getAllBooks();
  });
  
}

}