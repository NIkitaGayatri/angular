import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BooklistComponent } from './booklist/booklist.component';
import { BookComponent } from './book/book.component';
import{FormsModule}from '@angular/forms';
import{BookComponentRoutes} from '../books/book.routes'



@NgModule({
  declarations: [BooklistComponent, BookComponent],
  imports: [
    CommonModule,
    FormsModule,
    BookComponentRoutes

  ]
})
export class BooksModule { }
