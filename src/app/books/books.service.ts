import { Injectable } from '@angular/core';
import { ApphttpclientService } from 'src/app/apphttpclient.service';
import {Book} from '../books/book';
@Injectable({
  providedIn: 'root'
})
export class BooksService {

  constructor(private httpClientService : ApphttpclientService) { }
  books:Book[];
  getBooksList(){
   return  this.httpClientService.get('books');
}
}
