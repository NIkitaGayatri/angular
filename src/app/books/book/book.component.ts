import { Component, OnInit ,EventEmitter, Output, Input} from '@angular/core';
import { Book } from '../book';
import { ApphttpclientService } from 'src/app/apphttpclient.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  constructor(private httpClientService:ApphttpclientService) { }
  
  ngOnInit(): void {
  }
  @Input()
  book : Book;
  

  @Input()
  editable:boolean; 

  @Output()
  successEvent = new EventEmitter<any>();

  saveBook(){
    if(this.editable){
      this.httpClientService.put("book/"+this.book.id,this.book).subscribe(res=>{
           console.log("sucesss In edit ");
          this.successEvent.emit("success");
        });
    }else{
      console.log(this.book.genre+this.book.author+"  "+this.book.bookName+"  ");
      this.httpClientService.post("book",this.book).subscribe(res=>{
        console.log("sucesss in create");
        this.successEvent.emit("success");
      });
    }
  }
  

}
